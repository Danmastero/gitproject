import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main (String[] args)
    {
        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());
        int[] tablica = new int[100];

        for (int i = 0;i<100;i++) {
            tablica[i] = rand.nextInt(100);
            System.out.println(tablica[i]);
        }

        Arrays.sort(tablica);
        System.out.println("Posortowana tablica :");
        for (int i = 0;i<100;i++) {
            System.out.println(tablica[i]);
        }
        System.out.println("Najwieksza wartosc to : " + tablica[tablica.length-1]);
        System.out.println("Najmniejsza wartosc to : " + tablica[0]);

    }
}
